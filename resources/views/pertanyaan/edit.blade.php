@extends('adminlte.master')

@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Question {{ $question->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{ $question->id }}" method="POST">
      @csrf
      @method('PATCH')
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $question->judul ) }}" placeholder="Input Judul">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <textarea class="form-control" id="isi" name="isi" cols="3" rows="3" placeholder="Input Isi Pertanyaan">{{ old('isi', $question->isi ) }}</textarea>
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
@endsection