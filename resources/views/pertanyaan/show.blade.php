@extends('adminlte.master')

@section('content')
  <div class="container-fluid">
    <div class="card mb-2 mt-2" style="width: 18rem;">
      <div class="card-header" style="background-color: rgba(0,0,0,.03)">
        {{ $question->judul }}
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">{{ $question->isi }}</li>
      </ul>
    </div>
    <a href="/pertanyaan" class="btn btn-primary ">Kembali</a>
  </div>

@endsection