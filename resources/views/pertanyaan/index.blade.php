@extends('adminlte.master')

@section('content')

  @if (session('success'))
    <div class="alert alert-success mb-2 mt-2" role="alert">
      {{ session('success') }}
    </div>
  @endif
  <a href="/pertanyaan/create" class="btn btn-primary mt-2 mb-2">Create New Question</a>
  <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tabel Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body p-0">
        
        <table class="table table-striped">
          <thead>
            <tr>
              <th style="width: 10px">No</th>
              <th>Judul</th>
              <th>Isi Pertanyaan</th>
              <th style="width: 60px">Actions</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($questions as $key => $question)
              <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $question->judul }}</td>
                <td>{{ $question->isi }}</td>
                <td style="display: flex;">
                  <a href="/pertanyaan/{{ $question->id }}" class="btn btn-info btn-sm">show</a>
                  <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-secondary btn-sm">edit</a>
                  <form action="/pertanyaan/{{ $question->id }}" method="POST" onsubmit="return confirm('Yakin ingin menghapus data ini?')">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="4" align="center">No Record Found</td>
              </tr>
          
            @endforelse

          </tbody>
        </table>
      </div>
    <!-- /.card-body -->
  </div>
@endsection