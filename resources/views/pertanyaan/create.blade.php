@extends('adminlte.master')

@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Question</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul') }}" placeholder="Input Judul">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi Pertanyaan</label>
          <textarea class="form-control" id="isi" name="isi" cols="3" rows="3" placeholder="Input Isi Pertanyaan">{{ old('isi') }}</textarea>
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
@endsection