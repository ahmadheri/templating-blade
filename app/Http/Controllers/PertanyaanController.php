<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $questions = DB::table('pertanyaan')->get();
        
        return view('pertanyaan.index', compact('questions'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    public function show($id)
    {
        $question = DB::table('pertanyaan')->where('id', $id)->first();
        
        return view('pertanyaan.show', compact('question'));
    }

    public function edit($id)
    {
        $question = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('question'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $question = DB::table('pertanyaan')
                        ->where('id', $id)
                        ->update([
                            'judul' => $request->judul,
                            'isi' => $request->isi
                        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diupdate!');
    }

    public function destroy($id)
    {
        $question = DB::table('pertanyaan')->where('id', $id)->delete();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
