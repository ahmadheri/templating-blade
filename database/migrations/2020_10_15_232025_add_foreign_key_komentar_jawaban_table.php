<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komentar_jawaban', function(Blueprint $table){
            $table->unsignedBigInteger('jawaban_id');
            $table->unsignedBigInteger('profile_id');
            
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar_jawaban', function(Blueprint $table){
            $table->dropForeign(['profile_id', 'jawaban_id']);
            $table->dropColumn(['profile_id', 'jawaban_id']);
        });
    }
}
